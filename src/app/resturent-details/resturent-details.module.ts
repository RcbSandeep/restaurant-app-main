import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResturentDetailsPageRoutingModule } from './resturent-details-routing.module';

import { ResturentDetailsPage } from './resturent-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResturentDetailsPageRoutingModule
  ],
  declarations: [ResturentDetailsPage]
})
export class ResturentDetailsPageModule {}
