import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/home', icon: 'mail' },
    { title: 'Resturent details', url: '/resturent-details', icon: 'paper-plane' },
    { title: 'Trending', url: './trending', icon: 'heart' },
    { title: 'Order', url: '/order', icon: 'archive' },
    { title: 'Recipe', url: '/recipe', icon: 'trash' },
    { title: 'Payment', url: '/payment', icon: 'card' },
    { title: 'Contact us', url: '/contact-us', icon: 'contact' },
    { title: 'Forgot Password', url: '/forgot', icon: 'warning' },
    { title: 'login', url: '/login', icon: 'log-in' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() { }
}
